#pragma once

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <ros/ros.h>

#include <string>

#include "libtrisect/v4l_camera.h"
#include "trisect_camera_driver/trisect_camera_publisher.h"

namespace trisect_camera_driver {

class MonoCameraHandler {
 public:
  MonoCameraHandler(ros::NodeHandle &nh, ros::NodeHandle &pnh,
                    const std::string &name,
                    const std::shared_ptr<libtrisect::V4LCamera> &camera,
                    diagnostic_updater::Updater &updater,
                    bool accelerated_image_proc = true);

  std::shared_ptr<libtrisect::V4LCamera> camera() { return camera_; }

  int count() { return image_pub_->count(); }

  void start() {
    if (camera()) camera()->start();
  }

  void stop() {
    if (camera()) {
      camera()->setDone();
      camera()->join();
    }
  }

  void setDownsampleSigma(float sigma) {
    if (downsampled_pub_) downsampled_pub_->setSigma(sigma);
  }

 private:
  std::string name_;

  std::shared_ptr<libtrisect::V4LCamera> camera_;
  std::shared_ptr<TrisectCameraPublisher> image_pub_;
  std::shared_ptr<UndistortPublisher> undistort_pub_;
  std::shared_ptr<DownsamplePublisher> downsampled_pub_;
  std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager_;

  double min_freq_, max_freq_;
  std::shared_ptr<diagnostic_updater::HeaderlessTopicDiagnostic> pub_freq_;

  image_transport::ImageTransport image_transport_;

  ros::Publisher metadata_pub_;
};

}  // namespace trisect_camera_driver
