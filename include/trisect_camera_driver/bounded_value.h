#pragma once

#include "libtrisect/utils/min_max.h"

namespace trisect_camera_driver {

// AutoMinMax extends MinMax by adding a state variable
template <typename T>
struct BoundedValue : public libtrisect::MinMax<T> {
  typedef T Type;

  BoundedValue(const libtrisect::MinMax<T> &other, T initial = 0)
      : libtrisect::MinMax<T>(other), _value(this->_min) {
    set(initial);
  }

  T value() const { return _value; }
  void set(T val) { _value = this->bound(val); }

 private:
  T _value;
};

}  // namespace trisect_camera_driver
