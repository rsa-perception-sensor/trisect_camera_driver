#pragma once

#include <functional>
#include <string>
#include <thread>
#include <vector>
using namespace std;

#include <camera_info_manager/camera_info_manager.h>
#include <image_transport/image_transport.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <vpi/Image.h>
#include <vpi/LensDistortionModels.h>
#include <vpi/Stream.h>
#include <vpi/WarpMap.h>

#include <opencv2/core.hpp>

#include "libtrisect/gst_camera.h"
#include "libtrisect/v4l_camera.h"

namespace trisect_camera_driver {

class TrisectCameraPublisher {
 public:
  TrisectCameraPublisher(
      const std::string &name, image_transport::CameraPublisher pub,
      std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager);
  virtual ~TrisectCameraPublisher();

  virtual void publish(const libtrisect::Camera::Buffer_t &buffer);

  int count() const { return count_; }

  // Allow an external callback when a new image has been published
  typedef function<void(const std_msgs::Header &hdr)> HeaderOnlyCallback;
  typedef function<void(const libtrisect::Camera::Buffer_t &buffer)>
      OnPublishCallback;

  void addOnPublishCallback(HeaderOnlyCallback f) {
    on_publish_callbacks_.push_back(
        [this, f](const libtrisect::Camera::Buffer_t &buffer) {
          f(headerFromBuffer(buffer));
        });
  }

  void addOnPublishCallback(OnPublishCallback f) {
    on_publish_callbacks_.push_back(f);
  }

 protected:
  std_msgs::Header headerFromBuffer(const libtrisect::Camera::Buffer_t &buffer);

  image_transport::CameraPublisher image_publisher_;
  std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager_;

  VPIStream stream_;

  std::string name_;
  int count_;

  std::vector<OnPublishCallback> on_publish_callbacks_;
};

class UndistortPublisher : public TrisectCameraPublisher {
 public:
  UndistortPublisher(
      const std::string &name, image_transport::CameraPublisher pub,
      std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager);

  virtual ~UndistortPublisher();

  virtual void publish(const libtrisect::Camera::Buffer_t &buffer) override;

 private:
  void updateWarp(VPIImage input);

  bool initialized_;

  VPIImage output_;

  VPIPolynomialLensDistortionModel dist_model_;
  VPIWarpMap map_;
  VPIPayload warp_;
};

class DownsamplePublisher : public TrisectCameraPublisher {
 public:
  DownsamplePublisher(
      ros::NodeHandle &pnh, const std::string &name,
      image_transport::CameraPublisher pub,
      std::shared_ptr<camera_info_manager::CameraInfoManager> info_manager,
      int downsample, float gaussian_sigma);

  virtual ~DownsamplePublisher();

  virtual void publish(const libtrisect::Camera::Buffer_t &buffer) override;

  void setSigma(float sigma) { gaussian_sigma_ = sigma; }

 private:
  VPIImage ensure_output_exists(VPIImage input);
  VPIImage ensure_blurred_exists(VPIImage input);

  int downsample_;
  float gaussian_sigma_;

  VPIImage output_, blurred_;
};

}  // namespace trisect_camera_driver
