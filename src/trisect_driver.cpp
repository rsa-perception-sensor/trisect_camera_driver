
#include "trisect_camera_driver/trisect_driver.h"

#include "libtrisect/camera.h"
#include "libtrisect/trisect_cameras.h"
#include "trisect_msgs/TrisectImagingMetadata.h"

namespace trisect_camera_driver {

using namespace libtrisect;
using libtrisect::Camera;

TrisectDriver::TrisectDriver(
    ros::NodeHandle &nh, ros::NodeHandle &pnh,
    const std::shared_ptr<libtrisect::V4LCamera> &leftCam,
    const std::shared_ptr<libtrisect::V4LCamera> &rightCam,
    const std::shared_ptr<libtrisect::GstCamera> &colorCam,
    const std::shared_ptr<libtrisect::TriggerService> &triggerService,
    bool accelerated_image_proc)
    : _updater(),
      left_(nh, pnh, "left", leftCam, _updater, accelerated_image_proc),
      right_(nh, pnh, "right", rightCam, _updater, accelerated_image_proc),
      color_(nh, pnh, "color", colorCam, _updater),
      exposure_calculator_(),
      trigger_client_(triggerService),
      _reconfigureServer(new ReconfigureServer(pnh)),
      _colorPubFreq(),
      _minFreq(0.1),
      _maxFreq(35),
      _periodicTimer(),
      code_timing_(nh),
      image_transport_(nh) {
  libtrisect::setThreadName("trisect_camera_driver");

  exposure_calculator_ = make_shared<VPIExposureCalculator>(left_.camera());

  //~~ Handle exposure ~~

  bool link_exposures, auto_exposure;
  pnh.param<bool>("link_exposures", link_exposures, false);
  pnh.param<bool>("auto_exposure", auto_exposure, false);

  if (left_.camera() && right_.camera()) {
    if (auto_exposure) {
      // n.b. auto_exposure == True means that autoexposure is done
      // **in user space** in this driver.
      // auto_exposure == False uses the autoexposure
      // **provided by the camera hardware**
      //
      left_.camera()->setAutoExposure(false);
      right_.camera()->setAutoExposure(false);

      ROS_INFO("Enabling autoexposure calculation");

      int exposureTarget;
      pnh.param<int>("stereo_exposure_target", exposureTarget, 128);
      exposure_calculator_->setExposureTarget(exposureTarget);

      const float initialAutoExposureNs = 5000000;  //  5ms
      exposure_calculator_->exposure().set(initialAutoExposureNs);

      ROS_INFO_STREAM("Setting exposure max to "
                      << (exposure_calculator_->exposure().max() / 1e6)
                      << " ms");

      left_.camera()->addImageCallback([&](const Camera::Buffer_t &image) {
        auto timing = code_timing_.startBlock("calculate_exposure",
                                              "TrisectCameraDriver");

        auto v = exposure_calculator_->calculate(image);

        left_.camera()->setExposure(v);

        if (link_exposures) {
          right_.camera()->setExposure(v);
        }
      });

    } else {
      ROS_WARN("!!!! Using left camera's internal autoexposure");

      left_.camera()->setAutoExposure(true);
      left_.camera()->setExposure(5000000);

      if (link_exposures) {
        ROS_INFO("     and slaving right exposure to left camera.");
        auto err = rightCam->setAutoExposure(false);
        right_.camera()->addImageCallback([&](const Camera::Buffer_t &image) {
          right_.camera()->setExposure(left_.camera()->exposure());
        });
      } else {
        ROS_WARN("!!!! Using right camera's internal autoexposure");
        right_.camera()->setAutoExposure(true);
      }
    }
  }

  _reconfigureServer->setCallback(
      boost::bind(&TrisectDriver::reconfigureCallback, this, _1, _2));

  std::string hostname;
  pnh.param<std::string>("host", hostname, "(none)");
  _updater.setHardwareID(hostname);

  _periodicTimer = nh.createTimer(
      ros::Duration(1.0), boost::bind(&TrisectDriver::periodicTask, this, _1));
}

TrisectDriver::~TrisectDriver() { stop(); }

void TrisectDriver::configureCamera(
    ros::NodeHandle &pnh, const std::string &name,
    const std::shared_ptr<Camera> &cam,
    const std::shared_ptr<camera_info_manager::CameraInfoManager> &info_mgr,
    std::shared_ptr<TrisectCameraPublisher> &pub) {
  std::string calibration_topic(name);
  calibration_topic += "_camera_info";

  std::string calibrationUrl;
  pnh.param<std::string>(calibration_topic, calibrationUrl, "");
  ROS_INFO_STREAM(name << " camera using calibration URL: " << calibrationUrl);
  if (calibrationUrl.size() > 0) {
    bool success = info_mgr->loadCameraInfo(calibrationUrl);
  }

  std::string raw_topic(name);
  raw_topic += "/image_raw";

  pub = std::make_shared<TrisectCameraPublisher>(
      name, image_transport_.advertiseCamera(raw_topic, 1), info_mgr);
}

void TrisectDriver::start() {
  // \todo Check if already started
  left_.start();
  right_.start();
  color_.start();

  trigger_client_->start();
}

void TrisectDriver::stop() {
  ROS_WARN("Stopping TrisectDriver...");

  trigger_client_->stop();

  left_.stop();
  right_.stop();
  color_.stop();

  ROS_WARN("  ... done.");
}

void TrisectDriver::reconfigureCallback(
    trisect_camera_driver::TrisectCameraConfig &config, uint32_t level) {
  ROS_INFO("Processing reconfigure request");

  // if(  (level >=
  // (uint32_t)dynamic_reconfigure::SensorLevels::RECONFIGURE_STOP) )
  //     stop();

  if ((config.stereo_fps != _lastConfig.stereo_fps) ||
      (config.stereo_exposure_max_ms != _lastConfig.stereo_exposure_max_ms)) {
    trigger_client_->setFps(config.stereo_fps);

    const int32_t nsec = floor((1.0 / config.stereo_fps) * 1e9);

    float exposureMaxMs = config.stereo_exposure_max_ms;
    float exposureMaxNs = std::min(exposureMaxMs * 1e6, nsec * 0.8);

    ROS_INFO_STREAM("Setting autoexposure max to " << (exposureMaxNs / 1e6)
                                                   << " ms");
    exposure_calculator_->exposure().setMax(exposureMaxNs);
  }

  if (config.stereo_gain != _lastConfig.stereo_gain) {
    ROS_INFO_STREAM("Setting stereo camera gains to " << config.stereo_gain);
    if (left_.camera()) left_.camera()->setGain(config.stereo_gain);
    if (right_.camera()) right_.camera()->setGain(config.stereo_gain);
  }

  if (config.stereo_blur_sigma != _lastConfig.stereo_blur_sigma) {
    left_.setDownsampleSigma(config.stereo_blur_sigma);
    right_.setDownsampleSigma(config.stereo_blur_sigma);
  }

  exposure_calculator_->setExposureTarget(config.stereo_exposure_target);
  exposure_calculator_->setExposureKp(config.stereo_exposure_kp);

  _lastConfig = config;
}

void TrisectDriver::periodicTask(const ros::TimerEvent &event) {
  static auto last_l = 0;
  static auto last_r = 0;
  static auto last_color = 0;

  _updater.update();

  // \todo Implement some sort of watchdog?

  const auto l = left_.count();
  const auto r = right_.count();
  const auto color_count = color_.count();

  const auto dt = event.current_real - event.last_real;

  const int msg_period = 5;
  if (dt.toSec() > 0) {
    const auto dt_sec = dt.toSec();

    if (color_.camera()) {
      ROS_INFO_STREAM_THROTTLE(
          msg_period, "Mono camera "
                          << l << "/" << r << " frames (delta " << (l - r)
                          << "); Color " << color_count << " frames.  FPS: "
                          << std::setprecision(2) << std::fixed
                          << static_cast<float>(l - last_l) / dt_sec << ", "
                          << static_cast<float>(r - last_r) / dt_sec << ", "
                          << static_cast<float>(color_count - last_color) /
                                 dt_sec);
    } else {
      ROS_INFO_STREAM_THROTTLE(
          msg_period, "Mono camera "
                          << l << "/" << r << " frames (delta " << (l - r)
                          << ").  FPS: " << std::setprecision(2) << std::fixed
                          << static_cast<float>(l - last_l) / dt_sec << ", "
                          << static_cast<float>(r - last_r) / dt_sec);
    }
  } else {
    if (color_.camera()) {
      ROS_INFO_STREAM_THROTTLE(
          msg_period, "Mono camera " << l << "/" << r << " frames (delta "
                                     << (l - r) << "); Color " << color_count
                                     << " frames");
    } else {
      ROS_INFO_STREAM_THROTTLE(msg_period, "Mono camera " << l << "/" << r
                                                          << " frames (delta "
                                                          << (l - r));
    }
  }

  last_l = l;
  last_r = r;
  last_color = color_count;
}

}  // namespace trisect_camera_driver
