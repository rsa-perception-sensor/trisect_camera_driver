#include <image_proc/advertisement_checker.h>
#include <nodelet/loader.h>
#include <ros/ros.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "vpi_rectify_node");

  nodelet::Loader manager(false);  // Don't bring up the manager ROS API
  nodelet::M_string remappings;
  nodelet::V_string my_argv;

  // Rectify nodelet, image_mono -> image_rect
  std::string rectify_mono_name = ros::this_node::getName();
  manager.load(rectify_mono_name, "trisect_camera_driver/vpi_rectify",
               remappings, my_argv);

  ros::spin();
  return 0;
}
