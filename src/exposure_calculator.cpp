// For performance timing
#include <chrono>
using namespace std::chrono;

#include <ros/console.h>

#include <opencv2/core.hpp>
#include <vpi/OpenCVInterop.hpp>

#include "libtrisect/trisect_cameras.h"
#include "libtrisect/utils/set_thread_name.h"
#include "libtrisect/vpi_utils.h"
#include "trisect_camera_driver/exposure_calculator.h"

namespace trisect_camera_driver {

using libtrisect::Camera;

ExposureCalculator::ExposureCalculator(
    const libtrisect::V4LCamera::SharedPtr &camera,
    ExposureValue::Type initialExposure, GainValue::Type initialGain)
    : _calcExposure(camera->exposureBounds(), initialExposure),
      _calcGain(camera->gainBounds(), initialGain),
      exposure_kp_(1000000) {}

void ExposureCalculator::setExposureTarget(float target) {
  if ((target < 0) || (target > 1.0)) return;

  _exposureTarget = target;
}

void ExposureCalculator::setExposureKp(float kp) { exposure_kp_ = kp; }

//\todo.  This should be spun off into its own thread so it doesn't block
//        the main image acquisition...
libtrisect::Camera::ExposureType ExposureCalculator::calculate(
    const Camera::Buffer_t &buffer) {
  libtrisect::setThreadName("exposureCalculator");

  // \todo{} Fix for multiple depths
  int full_scale = UINT8_MAX;

  // int type;
  // if (image.pixformat == V4L2_PIX_FMT_GREY) {
  //   full_scale = UINT8_MAX;
  //   type = CV_8UC1;
  // } else if ((image.pixformat == libtrisect::MonoCamera::FORMAT_GREY10) ||
  //            (image.pixformat == libtrisect::MonoCamera::FORMAT_GREY12)) {
  //   full_scale = UINT16_MAX;
  //   type = CV_16UC1;
  // } else {
  //   ROS_WARN_STREAM_THROTTLE(1, "Can't handle pixformat "
  //                                   << image.pixformat << " using default of
  //                                   "
  //                                   << _calcExposure.value());
  //   return _calcExposure.value();
  // }

  VPIImageData img_data;
  memset(&img_data, 0, sizeof(img_data));
  CHECK_STATUS(vpiImageLockData(buffer.image, VPI_LOCK_READ,
                                VPI_IMAGE_BUFFER_HOST_PITCH_LINEAR, &img_data));

  cv::Mat mat;
  vpiImageDataExportOpenCVMat(img_data, &mat);

  const float mean = cv::mean(mat)[0];
  const float mean_val = mean / full_scale;
  ROS_DEBUG_STREAM_THROTTLE(
      1, "Mean = " << mean << " ; pct full range " << mean_val);

  vpiImageUnlock(buffer.image);

  const float err = (_exposureTarget - mean_val);

  // Simple P controller
  libtrisect::Camera::ExposureType delta = exposure_kp_ * err;

  delta =
      std::max(-MAX_DELTA_EXPOSURE_NS, std::min(MAX_DELTA_EXPOSURE_NS, delta));

  const auto exp = _calcExposure.value() + delta;
  _calcExposure.set(exp);

  ROS_DEBUG_STREAM_THROTTLE(
      1, "Brightness error = " << err << " ; requesting exposure " << exp
                               << " ; got actual exposure "
                               << _calcExposure.value());

  // ROS_INFO_STREAM_THROTTLE(
  //     1, "            kp = " << exposure_kp_ << " ; delta " << delta );

  return _calcExposure.value();
}

}  // namespace trisect_camera_driver
