
#include "libtrisect/camera.h"
#include "libtrisect/trisect_cameras.h"
#include "trisect_camera_driver/trisect_driver.h"

namespace trisect_camera_driver {

using namespace libtrisect;
using libtrisect::Camera;

TrisectDriver::FactoryExpected TrisectDriver::CreateTrisectDriver(
    ros::NodeHandle &nh, ros::NodeHandle &pnh) {
  // Steps which _might_ fail (largely interactions with the
  // camera hardware) are in this factory.
  //
  // This reduces the incidence of "failure to initialize" conditions
  // in the constructor.

  int gain;
  pnh.param<int>("stereo_gain", gain, 1200);

  bool is_drysect;
  pnh.param<bool>("is_drysect", is_drysect, false);

  bool do_left, do_right, do_color;
  pnh.param<bool>("do_left", do_left, true);
  pnh.param<bool>("do_right", do_right, true);
  pnh.param<bool>("do_color", do_color, true);

  int stereo_bit_depth;
  pnh.param<int>("stereo_bit_depth", stereo_bit_depth, 8);

  bool stereo_h_flip, stereo_v_flip;
  pnh.param<bool>("stereo_h_flip", stereo_h_flip, false);
  pnh.param<bool>("stereo_v_flip", stereo_v_flip, false);

  bool accelerated_image_proc;
  pnh.param<bool>("accelerated_image_proc", accelerated_image_proc, true);

  libtrisect::MonoCamera::MonoDataFormat_t monoFormat;
  if (stereo_bit_depth == 8) {
    ROS_INFO("Using 8-bit mono imagery");
    monoFormat = libtrisect::MonoCamera::FORMAT_GREY8;
  } else if (stereo_bit_depth == 10) {
    ROS_INFO("Using 10-bit mono imagery");
    monoFormat = libtrisect::MonoCamera::FORMAT_GREY10;
  } else if (stereo_bit_depth == 12) {
    ROS_INFO("Using 12-bit mono imagery");
    monoFormat = libtrisect::MonoCamera::FORMAT_GREY12;
  } else {
    Camera::CameraFactoryError err;
    err.stream() << "Undefined pixel bit depth \"" << stereo_bit_depth << "\"";
    return tl::make_unexpected(err);
  }

  std::shared_ptr<CamFactory> cam_factory;
  if (is_drysect) {
    ROS_INFO("Creating __drysect__ cameras");
    cam_factory = std::make_shared<DrysectCameras>();
  } else {
    ROS_INFO("Creating __trisect__ cameras");
    cam_factory = std::make_shared<TrisectCameras>();
  }

  if (!cam_factory) {
    Camera::CameraFactoryError err;
    err.stream() << "Unable to create cam_factory";
    return tl::make_unexpected(err);
  }

  auto left = cam_factory->LeftCamera(monoFormat);
  if (!left) {
    Camera::CameraFactoryError err;
    err.stream() << "Failed to create left camera (" << left.error().err
                 << "): " << left.error().msg();
    return tl::make_unexpected(err);
  }

  std::shared_ptr<libtrisect::V4LCamera> leftCam = left.value();

  {
    auto retval = leftCam->setAutoGain(false);
    if (retval) {
      Camera::CameraFactoryError err;
      err.stream() << "LEFT: Error disabling auto gain (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }

    ROS_INFO_STREAM("LEFT: Setting gain to " << gain);
    retval = leftCam->setGain(gain);
    if (retval) {
      Camera::CameraFactoryError err;
      err.stream() << "LEFT: Error setting gain (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }
  }

  {
    auto retval = leftCam->setAutoExposure(true);
    if (retval != 0) {
      Camera::CameraFactoryError err;
      err.stream() << "LEFT: Error setting auto exposure (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }
  }

  {
    auto retval = leftCam->setFlip(stereo_h_flip, stereo_v_flip);
    if (retval != 0) {
      Camera::CameraFactoryError err;
      err.stream() << "LEFT: Error setting camera rotation (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }
  }

  ROS_INFO("Completed initialization of left camera...");

  auto right = cam_factory->RightCamera(monoFormat);
  if (!right) {
    Camera::CameraFactoryError err;
    err.stream() << "Failed to create right camera (" << right.error().err
                 << "): " << right.error().msg();
    return tl::make_unexpected(err);
  }

  std::shared_ptr<libtrisect::V4LCamera> rightCam = right.value();

  {
    auto retval = rightCam->setAutoGain(false);
    if (retval) {
      Camera::CameraFactoryError err;
      err.stream() << "RIGHT: Error disabling auto gain (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }

    ROS_INFO_STREAM("RIGHT: Setting gain to " << gain);
    retval = rightCam->setGain(gain);
    if (retval) {
      Camera::CameraFactoryError err;
      err.stream() << "RIGHT: Error setting gain (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }
  }

  {
    auto retval = rightCam->setFlip(stereo_h_flip, stereo_v_flip);
    if (retval != 0) {
      Camera::CameraFactoryError err;
      err.stream() << "RIGHT: Error setting camera rotation (" << retval
                   << "): " << strerror(retval);
      return tl::make_unexpected(err);
    }
  }

  ROS_INFO("Completed initialization of right camera...");

  std::shared_ptr<libtrisect::GstCamera> colorCam;

  if (do_color) {
    int color_fps;
    pnh.param<int>("color_fps", color_fps, 10);
    ROS_INFO_STREAM("Starting color camera at " << color_fps << " hz");

    int color_resolution;
    pnh.param<int>("color_resolution", color_resolution,
                   static_cast<int>(ColorCamera::Resolution::RES_3840_2160));

    if ((color_resolution < 0) ||
        (color_resolution >=
         static_cast<int>(ColorCamera::Resolution::NUM_RES))) {
      Camera::CameraFactoryError err;
      err.stream() << "COLOR: Unknown resolution (" << color_resolution << ")";
      ;
      return tl::make_unexpected(err);
    }

    // \todo{amarburg}  Make color resolution configurable
    //   RES_4032_3040
    auto color = cam_factory->ColorCamera(
        static_cast<ColorCamera::Resolution>(color_resolution), color_fps);
    if (!color) {
      Camera::CameraFactoryError err;
      err.stream() << "Failed to create color camera (" << color.error().err
                   << "): " << color.error().msg();
      return tl::make_unexpected(err);
    }
    colorCam = color.value();

    ROS_INFO("Initialized color camera...");
  } else {
    ROS_WARN("!!! __NOT__ running color camera");
  }

  bool stereo_software_trigger;
  pnh.param<bool>("stereo_software_trigger", stereo_software_trigger, false);

  libtrisect::TriggerType trigger_type = TriggerType::TRIGGERD;

  if (stereo_software_trigger) {
    trigger_type = TriggerType::SOFTWARE;
  }

  std::shared_ptr<libtrisect::TriggerService> trigger_service =
      libtrisect::MakeTriggerService(trigger_type);

  if (!trigger_service) {
    Camera::CameraFactoryError err;
    err.stream() << "Failed to create trigger service";
    return tl::make_unexpected(err);
  }

  if (leftCam)
    trigger_service->addTrigger(leftCam->configureTrigger(trigger_type));

  if (rightCam)
    trigger_service->addTrigger(rightCam->configureTrigger(trigger_type));

  return std::shared_ptr<TrisectDriver>(
      new TrisectDriver(nh, pnh, leftCam, rightCam, colorCam, trigger_service,
                        accelerated_image_proc));
}

}  // namespace trisect_camera_driver
