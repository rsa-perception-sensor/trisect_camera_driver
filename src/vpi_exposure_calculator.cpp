// For performance timing
#include <chrono>
using namespace std::chrono;

#include <ros/console.h>
#include <vpi/Array.h>
#include <vpi/ArrayType.h>
#include <vpi/Image.h>
#include <vpi/Stream.h>
#include <vpi/algo/ImageStats.h>

#include <opencv2/core.hpp>

#include "libtrisect/trisect_cameras.h"
#include "libtrisect/utils/set_thread_name.h"
#include "libtrisect/vpi_utils.h"
#include "trisect_camera_driver/exposure_calculator.h"

namespace trisect_camera_driver {

using libtrisect::Camera;

VPIExposureCalculator::VPIExposureCalculator(
    const libtrisect::V4LCamera::SharedPtr &camera,
    ExposureValue::Type initialExposure, GainValue::Type initialGain)
    : ExposureCalculator(camera, initialExposure, initialGain) {
  CHECK_STATUS(vpiStreamCreate(0, &stream_));
  CHECK_STATUS(vpiArrayCreate(1, VPI_ARRAY_TYPE_STATISTICS, 0, &output_));
}

VPIExposureCalculator::~VPIExposureCalculator() {
  if (stream_) vpiStreamDestroy(stream_);
  if (output_) vpiArrayDestroy(output_);
}

libtrisect::Camera::ExposureType VPIExposureCalculator::calculate(
    const Camera::Buffer_t &buffer) {
  // libtrisect::setThreadName("vpiExposureCalculator");

  CHECK_STATUS(vpiSubmitImageStats(stream_, VPI_BACKEND_CUDA, buffer.image,
                                   output_, nullptr, VPI_STAT_MEAN));
  CHECK_STATUS(vpiStreamSync(stream_));

  VPIArrayData image_stat_data;
  CHECK_STATUS(vpiArrayLockData(output_, VPI_LOCK_READ,
                                VPI_ARRAY_BUFFER_HOST_AOS, &image_stat_data));
  assert(image_stat_data.buffer.aos.type == VPI_ARRAY_TYPE_STATISTICS);
  VPIStats *image_stats = (VPIStats *)image_stat_data.buffer.aos.data;

  const float full_scale = 255.0;

  const float mean = image_stats->mean[0];
  const float mean_val = mean / full_scale;
  ROS_DEBUG_STREAM_THROTTLE(1, image_stats->pixelCount
                                   << " pixels; Mean = " << mean
                                   << " ; pct full range " << mean_val);

  CHECK_STATUS(vpiArrayUnlock(output_));

  const float err = (_exposureTarget - mean_val);

  // Simple P controller
  libtrisect::Camera::ExposureType delta = exposure_kp_ * err;

  delta =
      std::max(-MAX_DELTA_EXPOSURE_NS, std::min(MAX_DELTA_EXPOSURE_NS, delta));

  const auto exp = _calcExposure.value() + delta;
  _calcExposure.set(exp);

  ROS_DEBUG_STREAM_THROTTLE(
      1, "Brightness error = " << err << " ; requesting exposure " << exp
                               << " ; got actual exposure "
                               << _calcExposure.value());

  return _calcExposure.value();
}

}  // namespace trisect_camera_driver
