# trisect_driver

ROS hardware driver for the three-camera "Trisect" underwater camera system.

Contains the node / nodelet `trisect_driver` which is the ROS wrapper for the three camera systems

Much of the code for interfacing with the hardware cameras is contained in [libtrisect](https://gitlab.com/rsa-perception-sensor/libtrisect).  This package is primarily ROS subscribers and services.

# License

Released under the [BSD 3-clause license.](LICENSE)
